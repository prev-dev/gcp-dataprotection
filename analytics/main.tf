resource "google_project" "analytics_project" {
  name                = "analytics"
  project_id          = "${var.project_id}"
  org_id              = "${var.organization_id}"
  billing_account     = "${var.billing_account_id}"
  auto_create_network = false
}

resource "google_project_service" "gce_service" {
  project = "${google_project.analytics_project.project_id}"
  service = "compute.googleapis.com"
}

resource "google_compute_network" "analytics-network" {
  auto_create_subnetworks         = false
  delete_default_routes_on_create = false
  name                            = "analytics-network"
  project                         = "${google_project.analytics_project.project_id}"
  routing_mode                    = "REGIONAL"
  depends_on                      = ["google_project_service.gce_service"]
}

resource "google_compute_subnetwork" "analytics_subnet" {
  ip_cidr_range            = "10.30.0.0/24"
  name                     = "vpc-subnet"
  network                  = "${google_compute_network.analytics-network.self_link}"
  private_ip_google_access = false
  project                  = "${google_project.analytics_project.project_id}"
  region                   = "${var.region}"
}

