## Table of contents
* [General Info](#general-info)
* [Usage](#usage)
* [Technologies](#technologies)
* [Todo](#todo)

## General Info
This project started as an automated approach to deploying the architecture mentioned [here](https://cloud.google.com/architecture/automating-classification-of-data-uploaded-to-cloud-storage#gcloud) .
While that page is descriptive, it only provides the python script needed for basic DLP functionality, this has greatly expanded on that.
It has evolved into a way to provide a rapid demo environment for multiple GCP services and functionality aligned with best practice.
The original Cloud Function has been modified to accept ENV vars as a way to ease the deployment with Terraform.
Some other changes include the addition of a projects to emulate onpremise and data analytics environemnts, CMEK for the "sensitive" GCS bucket , VPC-SC
and other best practices.

## Usage 

Ensure that you havea  recent version of Terraform installed on your system and that you have appropriate access and 
credentials to a GCP environment. Building and testing this environment will incur charges in your GCP account, please
make sure to read the code and understand what is being created.

- Clone the repository :
````
git clone https://gitlab.com/prev-dev/gcp-dataprotection.git
````
- Copy the terraform.tfvars.example file to terraform.tfvars and edit appropriately.
- Initialize Terraform so that it pulls down the appropriate modules
````
terraform init
````
- Check the output of a plan
````
terraform plan
````
- If everything looks satisfactory, go ahead and apply
````
terraform apply
````
- Terraform will begin to provision the environment at this point and report any errors that it encounters. 
- Once terraform has finished provisioning the infrastructure you can begin testing it with the included sample data. The DLP
functionality is triggered when files are uploaded into the landingzone GCS bucket. You will be able to inspect the
sensitive and nonsensitive buckets for the sorted files once the DLP jobs complete.  

- Move into the sample data directory:
```` 
cd sample_data
````
- Copy the files into your landingzone bucket using gsutil and replacing [YOUR_LANDINGZONE_BUCKET] with the name of the 
landingzone bucket that was provisioned earlier:
````
gsutil -m  cp * gs://[YOUR_QUARANTINE_BUCKET]/
````
- To clean up and remove all the infrastructure that was created, in the main repo directory you can run :
```` 
terraform destroy
````

## Technologies
This project relies on various GCP services including :
- Google Compute Engine
- Identity Aware Proxy  
- Cloud Functions
- Cloud DLP
- Cloud KMS
- Cloud Storage ( and CMEK )
- VPC Service Controls
- Big Query

In addition, Python is used to implement the serverless DLP orchestration and Terraform is used for automated infrastructure
provisioning.

## Todo
- Fully implement VPC-SC across all projects
- Create a better sample data set
- Implement a datawarehouse capability in BQ by automatically moving non-sensitive data into a BQ table
- Demonstrate a reporting/analytics job running from CF in one project targetting BQ in another
