// Used to randomly name projects to make re-running in the same org multiple times easier

resource "random_id" "onprem_project_random" {
  byte_length = 4
  prefix      = "onprem-"
}

resource "random_id" "landingzone_project_random" {
  byte_length = 4
  prefix      = "landingzone-"
}

resource "random_id" "datawarehouse_project_random" {
  byte_length = 4
  prefix      = "datawarehouse-"
}

resource "random_id" "analytics_project_random" {
  byte_length = 4
  prefix      = "analytics-"
}

