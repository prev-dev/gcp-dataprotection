// The below are used if you want to hardcode the project names
// Also see main.tf in the root directory and terraform.tfvars

//variable "onprem_project_id" {
//  description = "The ID of the onprem GCP project that is going to be created"
//}
//variable "landingzone_project_id" {
//description = "The ID of the landingzone project that is going to be created"
//}
//variable "datawarehouse_project_id" {
//  description = "The ID of the datawarehouse GCP project that is going to be created"
//}
//variable "analytics_project_id" {
//  description = "The ID of the analytics project that is going to be created"
//}

variable "credentials_path" {
  description = "Path to the service account .json file"
}

variable "organization_id" {
  description = "Organization ID, which can be found at `gcloud organizations list`"
}

variable "billing_account_id" {
  description = "Billing account ID to which the new project should be associated"
}

variable "region" {
  description = "GCP Region"
  default     = "us-central1"
}
