provider "google" {
  version     = "~> 3.53.0"
  credentials = "${file("${var.credentials_path}")}"
}

module "onprem" {
  source                   = "./onprem"
  project_id               = "${random_id.onprem_project_random.hex}"
  organization_id          = "${var.organization_id}"
  billing_account_id       = "${var.billing_account_id}"
  region                   = "${var.region}"
}

module "landingzone" {
  source                    = "./landingzone"
  project_id                = "${random_id.landingzone_project_random.hex}"
  organization_id           = "${var.organization_id}"
  billing_account_id        = "${var.billing_account_id}"
  region                    = "${var.region}"
}

module "datawarehouse" {
  source                    = "./datawarehouse"
  project_id                = "${random_id.datawarehouse_project_random.hex}"
  organization_id           = "${var.organization_id}"
  billing_account_id        = "${var.billing_account_id}"
  region                    = "${var.region}"
}

module "analytics" {
  source                    = "./analytics"
  project_id                = "${random_id.analytics_project_random.hex}"
  organization_id           = "${var.organization_id}"
  billing_account_id        = "${var.billing_account_id}"
  region                    = "${var.region}"
}