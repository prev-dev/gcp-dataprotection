// random bucket names
resource "random_id" "landingzone_bucket_random" {
  byte_length = 4
  prefix      = "landingzone-"
}
resource "random_id" "sensitive_bucket_random" {
  byte_length = 4
  prefix      = "sensitive-"
}
resource "random_id" "nonsensitive_bucket_random" {
  byte_length = 4
  prefix      = "nonsensitive-"
}
resource "random_id" "cloudfunction_bucket_random" {
  byte_length = 4
  prefix      = "cloudfunction-"
}
