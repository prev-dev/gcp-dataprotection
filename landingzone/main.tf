resource "google_project" "landingzone_project" {
  name                = "landingzone"
  project_id          = "${var.project_id}"
  org_id              = "${var.organization_id}"
  billing_account     = "${var.billing_account_id}"
  auto_create_network = false
}

resource "google_project_service" "gce_service" {
  project = "${google_project.landingzone_project.project_id}"
  service = "compute.googleapis.com"
}

resource "google_compute_network" "landingzone-network" {
  auto_create_subnetworks         = false
  delete_default_routes_on_create = false
  name                            = "landingzone-network"
  project                         = "${google_project.landingzone_project.project_id}"
  routing_mode                    = "REGIONAL"
  depends_on                      = ["google_project_service.gce_service"]
}

resource "google_compute_subnetwork" "landingzone_subnet" {
  ip_cidr_range            = "10.10.0.0/24"
  name                     = "vpc-subnet"
  network                  = "${google_compute_network.landingzone-network.self_link}"
  private_ip_google_access = false
  project                  = "${google_project.landingzone_project.project_id}"
  region                   = "${var.region}"
}

// create bucket for cloudfunction archive
resource "google_storage_bucket" "cloudfunction" {
  name          = "${random_id.cloudfunction_bucket_random.hex}"
  project       = "${google_project.landingzone_project.project_id}"
  location      = "us-central1"
  force_destroy = true
  uniform_bucket_level_access = true
}

// create the buckets for initial uploads
resource "google_storage_bucket" "landingzone" {
  name          = "${random_id.landingzone_bucket_random.hex}"
  project       = "${google_project.landingzone_project.project_id}"
  location      = "us-central1"
  force_destroy = true
  uniform_bucket_level_access = true
}

resource "google_storage_bucket" "sensitive" {
  name          = "${random_id.sensitive_bucket_random.hex}"
  project       = "${google_project.landingzone_project.project_id}"
  location      = "us-central1"
  force_destroy = true
  uniform_bucket_level_access = true
  encryption {
    default_kms_key_name = "${google_kms_crypto_key.sensitive-key.id}"
  }
  depends_on = [google_kms_crypto_key_iam_binding.binding]
}

resource "google_storage_bucket" "nonsensitive" {
  name          = "${random_id.nonsensitive_bucket_random.hex}"
  project       = "${google_project.landingzone_project.project_id}"
  location      = "us-central1"
  force_destroy = true
  uniform_bucket_level_access = true
}


// create the pubsub topic to handle messagin from dlp to cloudfunctions
resource "google_pubsub_topic" "landingzone" {
  name = "landingzone"
  project = "${google_project.landingzone_project.project_id}"
  labels = {
    purpose = "dlp"
  }
}

// create the pubsub subscription for the cloud function to trigger on
resource "google_pubsub_subscription" "landingzone" {
  name = "landingzone"
  project = "${google_project.landingzone_project.project_id}"
  topic = google_pubsub_topic.landingzone.name
  labels = {
    purpose = "dlp"
  }
}

// setup the infra for the cloudfunctions

# Compress source code
data "archive_file" "source" {
  type        = "zip"
  source_dir  = "landingzone/src/"
  output_path = "landingzone/src/function.zip"
}

// Add source code zip to bucket
resource "google_storage_bucket_object" "zip" {
  # Append file MD5 to force bucket to be recreated
  name   = "source.zip#${data.archive_file.source.output_md5}"
  bucket = google_storage_bucket.cloudfunction.name
  source = data.archive_file.source.output_path
}

# Enable Cloud Functions API
resource "google_project_service" "cf" {
  project = "${google_project.landingzone_project.project_id}"
  service = "cloudfunctions.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy         = false
}

# Enable Cloud Build API
resource "google_project_service" "cb" {
  project = "${google_project.landingzone_project.project_id}"
  service = "cloudbuild.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy         = false
}

# Enable Cloud DLP API
resource "google_project_service" "dlp" {
  project = "${google_project.landingzone_project.project_id}"
  service = "dlp.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy         = false
}

# Enable Cloud KMS API
resource "google_project_service" "kms" {
  project = "${google_project.landingzone_project.project_id}"
  service = "cloudkms.googleapis.com"
  disable_dependent_services = true
  disable_on_destroy         = false
}
# Create KMS Keyring and Key for sensitive bucket

resource "google_kms_key_ring" "keyring" {
  name     = "keyring"
  location = "us-central1"
  project = "${google_project.landingzone_project.project_id}"
  depends_on = [google_project_service.kms]
}

resource "google_kms_crypto_key" "sensitive-key" {
  name            = "crypto-key-sensitive"
  key_ring        = google_kms_key_ring.keyring.id
  rotation_period = "100000s"
  //lifecycle {
  //prevent_destroy = true
  //}
}

// deploy first cloudfunction that triggers off of bucket upload

resource "google_cloudfunctions_function" "create_DLP_job" {
  name = "create_DLP_job"
  project = "${google_project.landingzone_project.project_id}"
  depends_on = ["google_project_service.cf", "google_project_service.cb"]
  region = var.region
  runtime = "python37"
  source_archive_bucket = google_storage_bucket.cloudfunction.name
  source_archive_object = google_storage_bucket_object.zip.name
  event_trigger {
    event_type = "google.storage.object.finalize"
    resource = google_storage_bucket.landingzone.name
  }
  environment_variables = {
    LANDINGZONE_PROJECT_ID = "${google_project.landingzone_project.project_id}"
    LANDINGZONE_BUCKET = "${google_storage_bucket.landingzone.name}"
    SENSITIVE_BUCKET = "${google_storage_bucket.sensitive.name}"
    NONSENSITIVE_BUCKET = "${google_storage_bucket.nonsensitive.name}"
    PUB_SUB_TOPIC = "${google_pubsub_topic.landingzone.name}"
  }
}
// deploy second cloudfunction that triggers off of pubsub

resource "google_cloudfunctions_function" "resolve_DLP" {
  name    = "resolve_DLP"
  project = "${google_project.landingzone_project.project_id}"
  depends_on = ["google_project_service.cf", "google_project_service.cb"]
  region = var.region
  runtime = "python37"
  source_archive_bucket = google_storage_bucket.cloudfunction.name
  source_archive_object = google_storage_bucket_object.zip.name
  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource = google_pubsub_topic.landingzone.name
  }
  environment_variables = {
    LANDINGZONE_PROJECT_ID = "${google_project.landingzone_project.project_id}"
    LANDINGZONE_BUCKET = "${google_storage_bucket.landingzone.name}"
    SENSITIVE_BUCKET = "${google_storage_bucket.sensitive.name}"
    NONSENSITIVE_BUCKET = "${google_storage_bucket.nonsensitive.name}"
    PUB_SUB_TOPIC = "${google_pubsub_topic.landingzone.name}"
  }
}

// setup some delays to wait for the IAM roles to exist
resource "time_sleep" "iam_propagation" {
  create_duration = "300s"
}


resource "google_project_iam_binding" "appengine_dlp_admin" {
  project = "${google_project.landingzone_project.project_id}"
  depends_on = ["google_storage_bucket_object.fake", "time_sleep.iam_propagation"]
  role    = "roles/dlp.admin"
  members = [
    "serviceAccount:${google_project.landingzone_project.project_id}@appspot.gserviceaccount.com",
  ]
}

resource "google_project_iam_binding" "appengine_dlp_agent" {
  project = "${google_project.landingzone_project.project_id}"
  depends_on = ["google_storage_bucket_object.fake", "time_sleep.iam_propagation"]
  role    = "roles/dlp.serviceAgent"
  members = [
    "serviceAccount:${google_project.landingzone_project.project_id}@appspot.gserviceaccount.com",
  ]
}

//trick the dlp api into creating the SA by triggering the cloudfunction
//this will fail but the SA should be built after this
resource "google_storage_bucket_object" "fake" {
  name   = "source.zip#${data.archive_file.source.output_md5}"
  depends_on = ["google_cloudfunctions_function.create_DLP_job", "time_sleep.iam_propagation"]
  bucket = google_storage_bucket.landingzone.name
  source = data.archive_file.source.output_path
}

resource "google_project_iam_binding" "dlp_agent_viewer" {
project = "${google_project.landingzone_project.project_id}"
  depends_on = ["google_storage_bucket_object.fake", "time_sleep.iam_propagation"]
  role    = "roles/viewer"
  members = [
    "serviceAccount:service-${google_project.landingzone_project.number}@dlp-api.iam.gserviceaccount.com",
  ]
}

resource "google_project_iam_binding" "dlp_agent_publish" {
  project = "${google_project.landingzone_project.project_id}"
  depends_on = ["google_storage_bucket_object.fake", "time_sleep.iam_propagation"]
  role    = "roles/pubsub.publisher"
  members = [
    "serviceAccount:service-${google_project.landingzone_project.number}@dlp-api.iam.gserviceaccount.com",
  ]
}

// setup the gcs SA to use CMEK
data "google_storage_project_service_account" "gcs_account" {
  project = "${google_project.landingzone_project.project_id}"
}

resource "google_kms_crypto_key_iam_binding" "binding" {
  crypto_key_id = "${google_kms_crypto_key.sensitive-key.id}"
  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"
  members = ["serviceAccount:${data.google_storage_project_service_account.gcs_account.email_address}"]
}

// setup CF to use CMEK
//resource "google_kms_crypto_key_iam_binding" "cf-binding" {
//  crypto_key_id = "${google_kms_crypto_key.sensitive-key.id}"
//  role          = "roles/cloudkms.cryptoKeyEncrypterDecrypter"
//  members = ["serviceAccount:service-${google_project.landingzone_project.number}@gcf-admin-robot.iam.gserviceaccount.com"]
//}

