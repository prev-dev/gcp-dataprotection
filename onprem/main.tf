resource "google_project" "onprem_project" {
  name                = "onprem"
  project_id          = "${var.project_id}"
  org_id              = "${var.organization_id}"
  billing_account     = "${var.billing_account_id}"
  auto_create_network = false
}

resource "google_project_service" "gce_service" {
  project = "${google_project.onprem_project.project_id}"
  service = "compute.googleapis.com"
}

resource "google_compute_network" "onprem-network" {
  auto_create_subnetworks         = false
  delete_default_routes_on_create = false
  name                            = "onprem-network"
  project                         = "${google_project.onprem_project.project_id}"
  routing_mode                    = "REGIONAL"
  depends_on                      = ["google_project_service.gce_service"]
}

resource "google_compute_subnetwork" "onprem_subnet" {
  ip_cidr_range            = "10.0.0.0/24"
  name                     = "vpc-subnet"
  network                  = "${google_compute_network.onprem-network.self_link}"
  private_ip_google_access = true
  project                  = "${google_project.onprem_project.project_id}"
  region                   = "${var.region}"
}

//Deploy a FW rule for IAP SSH access
resource "google_compute_firewall" "iap" {
  name    = "allow-ssh-ingress-from-iap"
  network = "${google_compute_network.onprem-network.name}"
  project = "${google_project.onprem_project.project_id}"
  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  target_tags = ["onprem-host"]
}

// Deploy a basic VM to emulate onprem
resource "google_compute_instance" "onprem-host" {
  boot_disk {
    auto_delete = true
    device_name = "onprem-host"
    initialize_params {
      image = "debian-cloud/debian-10"
      size  = "10"
      type  = "pd-standard"
    }
  }
  can_ip_forward      = false
  deletion_protection = false
  labels              = {}
  machine_type        = "n1-standard-1"
  metadata            = {}
  name                = "on-prem-host"
  network_interface {
    network    = "${google_compute_network.onprem-network.self_link}"
    subnetwork = "${google_compute_subnetwork.onprem_subnet.self_link}"
  }
  project = "${google_project.onprem_project.project_id}"
  tags       = ["onprem-host"]
  zone       = "${var.region}-b"
  depends_on = ["google_project_service.gce_service"]
}
